<?php

namespace App\Console\Commands;

use App\Http\Controllers\CDRFileController;
use App\Rest\CDR\CDRCronUploader;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class IngestCDR extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ingest:cdr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ingest CDRs Cron Job';

    private $cdrCronUploader;

    /**
     * Create a new command instance.
     * @param CDRCronUploader $cronUploader
     */
    public function __construct(CDRCronUploader $cronUploader)
    {
        parent::__construct();
        $this->cdrCronUploader = $cronUploader;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = array();
        $approval = true;
        $path = getenv('CDRFolder');
        $completed_path = getenv('UploadedFolder');
        $error_path = getenv('ErrorFolder');
        $files = array_diff(scandir($path), array('.', '..'));

        if($files)
        {
            foreach ($files as $file)
            {
                $ext = pathinfo($file, PATHINFO_EXTENSION);

                if($ext == "xls" || $ext == "xlsx")
                {
                    $data = $this->excelToArray($path.'/'.$file);
                }
                else if($ext == "csv")
                {
                    $data = $this->csvToArray($path.'/'.$file);
                }
                foreach ($data as $item)
                {
                    if (!array_key_exists('Caller MSISDN', $item) || !array_key_exists('Callee MSISDN', $item)
                        || !array_key_exists('Duration', $item) || !array_key_exists('Action Type', $item)
                        || !array_key_exists('Caller IMEI', $item) || !array_key_exists('Callee IMEI', $item)
                        || !array_key_exists('Caller IMSI', $item) || !array_key_exists('Callee IMSI', $item)
                        || !array_key_exists('Base Station Position', $item) || !array_key_exists('EndLocation', $item)
                        || !array_key_exists('Latitude', $item) || !array_key_exists('service_type', $item) || !array_key_exists('Longitude', $item)
                        || !array_key_exists('CALLER ID/NAME LOOKUP', $item) || !array_key_exists('CALLEE ID/NAME LOOKUP', $item) )
                    {
                        rename($path.'/'.$file, $error_path.'/'.$file);
                        $approval = false;
                        exit();
                    }
                }
                if($approval == true)
                {

                    rename($path.'/'.$file, $completed_path.'/'.$file);
                    $this->cdrCronUploader->uploadCDR($data);

                }
            }
        }
    }

    protected function csvToArray($file, $delimiter = ',')
    {
        $filename = $file;
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    protected function excelToArray($file)
    {
        $cdr_data = array();
        $filename = $file;
        $data = Excel::load($filename, function($reader) {})->get();
        if(!empty($data) && $data->count())
        {
            foreach ($data as $key => $value)
            {
                $cdr_data[] = $value->toArray();
            }
        }
        return $cdr_data;
    }
}
