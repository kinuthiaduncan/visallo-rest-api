<?php

namespace App\Http\Controllers;

use App\Rest\Authentication\Auth;
use App\Rest\CDR\CallSpecificInfo;
use App\Rest\CDR\CdrRepository;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CDRFileController extends Controller
{
    private $cdrRepository;
    private $auth;
    private $callSpecificInfo;

    /**
     * CDRFileController constructor.
     * @param CdrRepository $cdrRepository
     * @param Auth $auth
     * @param CallSpecificInfo $callSpecificInfo
     */
    public function __construct(CdrRepository $cdrRepository, Auth $auth, CallSpecificInfo $callSpecificInfo)
    {
        $this->cdrRepository = $cdrRepository;
        $this->auth = $auth;
        $this->callSpecificInfo = $callSpecificInfo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
       $userData = $this->auth->loginUser();

        if($request->file('cdr')->getClientOriginalExtension() == "xlsx" || $request->file('cdr')->getClientOriginalExtension() == "xls" ) {
            $data = $this->excelToArray($request);
        }
        else if($request->file('cdr')->getClientOriginalExtension() == "csv" ){
            $data = $this->csvToArray($request);
        }
        foreach($data as $item)
        {
            $caller = $this->cdrRepository->uploadCaller($userData, $item);
            $callee = $this->cdrRepository->uploadCallee($userData, $item);
            $conn = $this->cdrRepository->connectCallerToCallee($userData, $item, $caller, $callee);
            $connection = $conn;
            $baseStation = $this->callSpecificInfo->uploadBaseStation($connection, $userData, $item);
            $latitude = $this->callSpecificInfo->uploadLatitude($connection, $userData, $item);
            $longitude = $this->callSpecificInfo->uploadLongitude($connection, $userData, $item);
            $dateTime = $this->callSpecificInfo->uploadDateTime($connection, $userData, $item);
            $duration = $this->callSpecificInfo->uploadDuration($connection, $userData, $item);
            $endLocation = $this->callSpecificInfo->uploadEndLocation($connection, $userData, $item);
            $callerImei = $this->callSpecificInfo->uploadCallerImei($connection, $userData, $item);
            $calleeImei = $this->callSpecificInfo->uploadCalleeImei($connection, $userData, $item);
            $properties = $this->callSpecificInfo->getProperties($userData, $connection);
            $this->callSpecificInfo->publishProperty($userData, $properties, $connection);
        }
        flash('Upload Successful!')->success();
        return redirect()->route('home.index');
    }

    /**
     * @param $request
     * @param string $delimiter
     * @return array|bool
     */
    public function csvToArray($request, $delimiter = ',')
    {
        $filename = $request->file('cdr');
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     * @param $request
     * @return array
     */
    public function excelToArray($request)
    {
        $cdr_data = array();
        $filename = $request->file('cdr');
        $path = $request->file('cdr')->getRealPath();
        $data = Excel::load($path, function($reader) {
        })->get();
        if(!empty($data) && $data->count()) {
            foreach ($data as $key => $value) {
                $cdr_data[] = $value->toArray();
            }
        }
        return $cdr_data;
    }
}
