<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rest\Authentication\Auth;

class LoginController extends Controller
{
    private $auth;

    /**
     * LoginController constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request)
    {
        $this->validate($request,[
            'username'=>'required',
            'password'=>'required'
        ]);

       $data =  $this->auth->loginUser($request);
       return view('home',['data'=>$data]);
    }
}
