<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 9/14/17
 * Time: 4:00 PM
 */

namespace App\Rest\Authentication;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;


class Auth
{
    public function loginUser()
    {
//        header("Access-Control-Allow-Origin: *");
        $client = new Client(['verify' => false,'cookies' => true ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/login', [
                'form_params' => [
                    'username' => getenv('VISALLO_USERNAME'),
                    'password' => getenv('VISALLO_PASSWORD'),
                ]
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
            exit();
        }
        $cookieJar = $client->getConfig('cookies');
        $csrf_response = $client->request('GET', $url.'/user/me');
        $userData = \GuzzleHttp\json_decode($csrf_response->getBody());

        if(!array_key_exists('currentWorkspaceId',$userData))
        {
            $userData->currentWorkspaceId = $this->createWorkspace($userData->csrfToken, $cookieJar);
        }
        $data = [
            'user' => $userData,
            'cookies' => $cookieJar
        ];
        return $data;
    }

    public function createWorkspace($csrf, $cookieJar)
    {
        $client = new Client(['verify' => false,'cookies' => $cookieJar ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/workspace/create', [
                'headers' => [
                    'Visallo-CSRF-Token' => $csrf,
                ],
                'form_params' => array(
                    'csrfToken' => $csrf,
                )
            ]);
        }

        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return \GuzzleHttp\json_decode($response->getBody())->workspaceId;
    }
}