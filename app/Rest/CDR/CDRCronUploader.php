<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 11/8/17
 * Time: 10:57 AM
 */

namespace App\Rest\CDR;


use App\Rest\Authentication\Auth;

class CDRCronUploader
{
    private $auth;
    private $cdrRepository;
    private $callSpecificInfo;

    public function __construct(Auth $auth, CdrRepository $cdrRepository, CallSpecificInfo $callSpecificInfo)
    {
        $this->cdrRepository = $cdrRepository;
        $this->auth = $auth;
        $this->callSpecificInfo = $callSpecificInfo;
    }

    public function uploadCDR($cdr)
    {
        $userData = $this->auth->loginUser();
        $data  = $cdr;
        foreach($data as $item)
        {
            $caller = $this->cdrRepository->uploadCaller($userData, $item);
            $callee = $this->cdrRepository->uploadCallee($userData, $item);
            $conn = $this->cdrRepository->connectCallerToCallee($userData, $item, $caller, $callee);
            $connection = $conn;
            $baseStation = $this->callSpecificInfo->uploadBaseStation($connection, $userData, $item);
            $latitude = $this->callSpecificInfo->uploadLatitude($connection, $userData, $item);
            $longitude = $this->callSpecificInfo->uploadLongitude($connection, $userData, $item);
            $dateTime = $this->callSpecificInfo->uploadDateTime($connection, $userData, $item);
            $duration = $this->callSpecificInfo->uploadDuration($connection, $userData, $item);
            $endLocation = $this->callSpecificInfo->uploadEndLocation($connection, $userData, $item);
            $callerImei = $this->callSpecificInfo->uploadCallerImei($connection, $userData, $item);
            $calleeImei = $this->callSpecificInfo->uploadCalleeImei($connection, $userData, $item);
            $properties = $this->callSpecificInfo->getProperties($userData, $connection);
            $this->callSpecificInfo->publishProperty($userData, $properties, $connection);
        }
    }
}