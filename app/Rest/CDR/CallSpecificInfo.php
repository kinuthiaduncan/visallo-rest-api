<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 11/6/17
 * Time: 12:16 PM
 */

namespace App\Rest\CDR;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class CallSpecificInfo
{
    public function uploadBaseStation($connection, $userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/edge/property', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => $connection,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'metadata' => '',
                    'propertyName' => "http://visallo.org/dev#baseStation",
                    "value" => $item['Base Station Position'],
                )
            ]);

        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $response;

    }

    public function uploadLatitude($connection, $userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/edge/property', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => $connection,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                    'propertyName' => "http://visallo.org/dev#latitude",
                    "value" => $item['Latitude'],
                )
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $response;

    }

    public function uploadLongitude($connection, $userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/edge/property', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => $connection,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                    'propertyName' => "http://visallo.org/dev#longitude",
                    "value" => $item['Longitude'],
                )
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $response;

    }

    public function uploadDateTime($connection, $userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/edge/property', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => $connection,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                    'propertyName' => "http://visallo.org/dev#dateAndTime",
                    "value" => $this->convertDate($item['Date']),
        )
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $response;

    }

    public function convertDate($date)
    {
        $date = ($date != '') ? Carbon::parse($date)->toDateTimeString() : '';
        return $date;
    }

    public function uploadDuration($connection, $userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/edge/property', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => $connection,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                    'propertyName' => "http://visallo.org/dev#duration",
                    "value" => $item['Duration'],
                )
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $response;

    }
    public function uploadEndLocation($connection, $userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/edge/property', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => $connection,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                    'propertyName' => "http://visallo.org/dev#endLocation",
                    "value" => $item['EndLocation'],
                )
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            dd($response);
        }
        return $response;

    }

    public function uploadCallerImei($connection, $userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/edge/property', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => $connection,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                    'propertyName' => "http://visallo.org/dev#callerImei",
                    "value" => $item['Caller IMEI'],
                )
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $response;

    }

    public function uploadCalleeImei($connection, $userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/edge/property', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => $connection,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                    'propertyName' => "http://visallo.org/dev#calleeImei",
                    "value" => $item['Callee IMEI'],
                )
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $response;

    }

    public  function getProperties($userData, $edge)
    {
        $data = [];
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('GET', $url . '/edge/properties', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'query' => [
                    'graphEdgeId' => $edge
                ]
            ]);
            $data =\GuzzleHttp\json_decode($response->getBody()->getContents())->properties;
            unset($data[1]); unset($data[0]); unset($data[2]); unset($data[3]);unset($data[4]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $data;
    }


    public function publishProperty($userData, $properties, $edge)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        foreach ($properties as $property) {
            try {
                $response = $client->request('POST', $url . '/workspace/publish', [
                    'headers' => [
                        'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                    ],
                    'form_params' => array(
                        'csrfToken' => $userData['user']->csrfToken,
                        'publishData' => '[{"type":"property",
                     "action":"update","status":"PUBLIC","key":"' . $property->key . '","name":"' . $property->name . '"
                     ,"edgeId":"' . $edge. '"}]'
                    )
                ]);
            } catch (RequestException $e) {
                $response = $e->getResponse()->getBody()->getContents();
                dd($response);
                Log::info($response);
            }
        }
    }
}