<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 9/28/17
 * Time: 1:21 PM
 */
namespace App\Rest\CDR;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Webpatser\Uuid\Uuid;

class CdrRepository
{

    public function uploadCaller($userData, $item)
    {
       $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
       $url = getenv('VISALLO_URL');
       try {
           $response = $client->request('POST', $url . '/vertex/new', [
               'form_params' => [
                   'csrfToken' => $userData['user']->csrfToken,
                   'currentWorkspaceId' => $userData['user']->currentWorkspaceId,
                   'vertexId' => $this->determineCallerPerson($item),
                   'conceptType' => 'http://visallo.org/dev#person',
                   'justificationText' => 'cdr',
                   'visibilitySource' => '',
                   'publish' => 'true',
                   'properties' => '{"properties":[
                                    {"propertyName":"http://visallo.org#title",
                                    "value":"'.$this->determineCallerPerson($item).'","propertyKey":" "},
                                    {"propertyName":"http://visallo.org/dev#phoneNumber",
                                    "value":"'.$item['Caller MSISDN'].'","propertyKey":" "}
                                    ]}',
               ]
           ]);
       }
       catch (RequestException $e) {
           $response = $e->getResponse()->getBody()->getContents();
           Log::info($response);
       }
       return $response;
    }

    public function determineCallerPerson($item)
    {
        $person = ($item['CALLER ID/NAME LOOKUP'] != '') ? $item['CALLER ID/NAME LOOKUP'] : 'unknown - '.$item['Caller MSISDN'];
        return $person;
    }

    public function determineCalleePerson($item)
    {
        $person = ($item['CALLEE ID/NAME LOOKUP'] != '') ? $item['CALLEE ID/NAME LOOKUP'] : 'unknown - '.$item['Callee MSISDN'];
        return $person;
    }

    public function uploadCallee($userData, $item)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/vertex/new', [
                'form_params' => [
                    'csrfToken' => $userData['user']->csrfToken,
                    'currentWorkspaceId' => $userData['user']->currentWorkspaceId,
                    'vertexId' => $this->determineCalleePerson($item),
                    'conceptType' => 'http://visallo.org/dev#person',
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                    'properties' => '{"properties":[
                                    {"propertyName":"http://visallo.org#title",
                                    "value":"'.$this->determineCalleePerson($item).'","propertyKey":" "},
                                    {"propertyName":"http://visallo.org/dev#phoneNumber",
                                    "value":"'.$item['Callee MSISDN'].'","propertyKey":" "}
                                    ]}',
                ]
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
        }
        return $response;
    }

    public function connectCallerToCallee($userData, $item, $caller, $callee)
    {
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
        $url = getenv('VISALLO_URL');
        $caller = \GuzzleHttp\json_decode($caller->getBody()->getContents())->id ;
        $callee = \GuzzleHttp\json_decode($callee->getBody()->getContents())->id;
        $concept = $this->determineConcept($item);
        try {
            $response = $client->request('POST', $url . '/edge/create', [
                'headers' => [
                    'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                ],
                'form_params' => array(
                    'csrfToken' => $userData['user']->csrfToken,
                    'edgeId' => Uuid::generate()->string,
                    'outVertexId' => $this->determineCaller($item, $caller, $callee),
                    'inVertexId' => $this->determineCallee($item, $caller, $callee),
                    'predicateLabel' => $concept,
                    'conceptType' => $concept,
                    'justificationText' => 'cdr',
                    'visibilitySource' => '',
                    'publish' => 'true',
                )
            ]);
        }
        catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::info($response);
            }
        $edge = \GuzzleHttp\json_decode($response->getBody()->getContents())->id;
        $this->publishEdge($userData,$edge);

        return $edge;
    }

    public function determineConcept($item)
    {
        $concept = '';
        if(strtolower($item['service_type'])=='sms'){
            $concept = 'http://visallo.org/dev#sentSms';
        }
        else if(strtolower($item['service_type'])=='forwarded'){
            $concept = 'http://visallo.org/dev#forwarded';
        }
        else{
            $concept = 'http://visallo.org/dev#called';
        }
        return $concept;
    }

    public function determineCaller($item, $target, $associate)
    {
        $caller = '';
        if($item['Action Type'] =='Outgoing' || $item['Action Type'] =='outgoing' || $item['Action Type'] =='Calling' || $item['Action Type'] =='calling' || $item['Action Type'] =='Forwarded'|| $item['Action Type'] =='forwarded'|| $item['Action Type'] =='OUTGOING'|| $item['Action Type'] =='CALLING'|| $item['Action Type'] =='FORWARDED'|| $item['Action Type'] =='')
        {
            $caller = $target;
        }
        else
        {
            $caller = $associate;
        }
        return $caller;
    }
    public function determineCallee($item, $target, $associate)
    {
        $callee = '';
        if($item['Action Type'] =='Outgoing' || $item['Action Type'] =='outgoing' || $item['Action Type'] =='Calling' || $item['Action Type'] =='calling' || $item['Action Type'] =='Forwarded'|| $item['Action Type'] =='forwarded'|| $item['Action Type'] =='OUTGOING'|| $item['Action Type'] =='CALLING'|| $item['Action Type'] =='FORWARDED'|| $item['Action Type'] =='')
        {
            $callee = $associate;
        }
        else
        {
            $callee = $target;
        }
        return $callee;

    }

     public function publishEdge($userData, $edge)
     {
         $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);
         $url = getenv('VISALLO_URL');
         try {
             $response = $client->request('POST', $url . '/workspace/publish', [
                 'headers' => [
                     'Visallo-Workspace-Id' => $userData['user']->currentWorkspaceId,
                 ],
                 'form_params' => array(
                     'csrfToken' => $userData['user']->csrfToken,
                     'publishData'=>'[{"type":"relationship",
                     "action":"update","status":"PUBLIC","edgeId":"'.$edge.'"}]'
                 )
             ]);

         }
         catch (RequestException $e) {
             $response = $e->getResponse()->getBody()->getContents();
             Log::info($response);
         }
         return $response;
     }
}
