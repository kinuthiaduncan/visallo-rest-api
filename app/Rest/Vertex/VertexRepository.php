<?php
/**
 * Created by PhpStorm.
 * User: panther
 * Date: 9/19/17
 * Time: 3:26 PM
 */
namespace App\Rest\Vertex;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use App\Rest\Authentication\Auth;
use GuzzleHttp\Exception\RequestException;

class VertexRepository
{
    private $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function create($request)
    {
        $userData = $this->auth->loginUser();
        $client = new Client(['verify' => false,'cookies' => $userData['cookies'] ]);

        $url = getenv('VISALLO_URL');
        try {
            $response = $client->request('POST', $url . '/vertex/new', [
                'form_params' => [
                    'csrfToken' => $userData['user']->csrfToken,
                    'currentWorkspaceId' => $userData['user']->currentWorkspaceId,
                    'vertexId' => $request->vertex_id,
                    'conceptType' => $request->conceptType,
                    'justificationText' => $request->JustificationText,
                    'visibilitySource' => '',
                    'publish' => $request->publish,
                    'properties' => '{"properties":[{"propertyName":"'.$request->propertyName.'",
                                    "value":"'.$request->propertyValue.'","propertyKey":""}]}',
                ]
            ]);
        }catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
        }
        return $response;
    }
}