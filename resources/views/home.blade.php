<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CDR Upload</title>
    <link href="/css/app.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <style>
        .flash{
            color: green;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="flash">
            @include('flash::message')
        </div>
        <div class="upload-form">
            {!! Form::open(['files'=>'true', 'route'=>'upload-cdr']) !!}
            {!! Form::file('cdr',['required']) !!}
            <p class="desc">Drag your file here or click in this area.</p>
            {!! Form::submit('Upload CDR',['class'=>'sub']) !!}
            {!! Form::close() !!}
        </div>
    </div>

<script>
    $('#flash-overlay-modal').modal();
</script>
<script>
    $(document).ready(function(){
        $('form input').change(function () {
            $('form p').text(this.files.length + " file selected");
        });
    });
</script>
</body>
</html>



