<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::post('/','LoginController@login')->name('visallo_login');

Route::resource('/','HomeController',[
    'names'=>[
        'index'=>'home.index'
    ]
]);
Route::resource('vertex', 'VertexController');
Route::post('/cdr-file', 'CDRFileController@store')->name('upload-cdr');